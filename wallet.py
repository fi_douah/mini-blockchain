import socket,sys
import argparse
import pickle

HOST='127.0.0.1'

# get the argument from the console ( host port number )
parser = argparse.ArgumentParser()
parser.add_argument("port", type=int,help="numero de port hote")    
args = parser.parse_args()

PORT_MINER=args.port

# 1) Socket creation :
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

try:
    s.connect((HOST,PORT_MINER))

except socket.error:
    print("La liaison du socket à l'adresse choisie a échoué.")
    sys.exit

# push messages (transactions to the miner )
while 1 :
    print('>>>')
    msg = input() 
    s.send(pickle.dumps(msg))