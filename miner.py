import socket,sys
import pickle
import argparse


#Reading the paramaters from terminal 
parser = argparse.ArgumentParser()
parser.add_argument("port", type=int,help="numero de port hote")
parser.add_argument("-r","--register",type=int, help="numero de port register : -register PORT")

args = parser.parse_args()


#Global variabls
HOST='127.0.0.1'
PORT=args.port
PORT_REGISTER=args.register

#The list of the node connected to the P2P network
P2P=[]

#The list contain the hash of messages received by a node 
msg_recu=[]


#First connection of the node to the network
if(PORT_REGISTER):
    tmp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tmp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    tmp.bind((HOST, PORT))
    tmp.connect((HOST, PORT_REGISTER))
    tmp.send(pickle.dumps('Miner'))
    msgReceived = pickle.loads(tmp.recv(4096))
    P2P+=msgReceived
    P2P.append((HOST,PORT_REGISTER))
    tmp.close()


# 1) Creat socket:
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 2) Link the socket:
try:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((HOST, PORT))
except socket.error:
    print("La liaison du socket à l'adresse choisie a échoué.")
    sys.exit


s.listen()  
print("Serveur prêt, en attente de requêtes ...")

while 1:

    # 3) waiting for connection of miner or wallet:
    conn, addr = s.accept()
    msg=pickle.loads(conn.recv(4096))

    #Case msg from wallet
    if( msg != "Miner" and hash(msg) not in msg_recu):
        print('message recu : ',msg)
        msg_recu.append(hash(msg))
        for e in P2P :
            tmp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            tmp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try :
                tmp.connect(e)
                tmp.send(pickle.dumps(msg))
            except:
                print(e," est déconnecté ")
                P2P.remove(e)
            tmp.close()
        print("Serveur prêt, en attente de requêtes ...")

    #Case conenction of a new miner
    elif(msg == "Miner" and addr not in P2P):   
        print('Ajout du noeud :', addr)
        conn.send(pickle.dumps(P2P))
        P2P.append(addr)
        print("Serveur prêt, en attente de requêtes ...")
    
