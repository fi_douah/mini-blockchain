from hashlib import sha256
from datetime import datetime

class Block(object):
    def __init__(self, index, previousHash, timestamp, data):
        self.index = index
        self.previousHash = previousHash
        self.timestamp = timestamp
        self.data = data
        self.nonce = 0
        self.hash = Block.calculateHash(self)

    def mine_Block(self, difficulty):
        while not self.hash[0:difficulty].startswith('0' * difficulty) :
            self.nonce = self.nonce + 1
            self.hash = Block.calculateHash(self)

    @staticmethod
    def calculateHash(block):
        bloc = str(block.index) + str(block.previousHash) + str(block.timestamp) + str(block.data) + str(block.nonce)
        return(sha256(bloc.encode('utf-8')).hexdigest())